//
//  DetailTableViewCell.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import SDWebImage

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var lbFullname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setup(model: PullRequest) {
        lbTitle.text = model.title
        lbDesc.text = model.body
        imgAvatar.sd_setImage(with: URL(string: (model.owner!.avatar)!), placeholderImage: UIImage(named: "avatar"))
        
        lbUsername.text = model.owner!.login
        lbFullname.text = "nao tem"

    }
}
