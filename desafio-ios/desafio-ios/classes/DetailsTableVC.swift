//
//  DetailsTableVC.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class DetailsTableVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableview: UITableView!
    
    var repository: Repositories!
    
    let detailIdentier = "detailsCell"

    var feedList:[PullRequest] = []
    
    var indicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = repository.name

        self.tableview.estimatedRowHeight = 180
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        
        self.tableview.dataSource = self
        
        self.tableview.delegate = self
        
        self.tableview.tableFooterView = UIView()
        
        activityIndicator()
        
        indicator.startAnimating()
        
        indicator.backgroundColor = UIColor.white
        
        loadData { (list, error) in
            
            if list.count > 0 {
                self.feedList = list
                
                self.tableview.reloadData()
                
                self.indicator.stopAnimating()                
                self.indicator.hidesWhenStopped = true

            }
        }
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func loadData(callback: @escaping (_ pullResquest: [PullRequest], _ error: NSError?) -> ()) -> () {
        

        let URL = "https://api.github.com/repos/\(repository.owner!.login!)/\(repository.name!)/pulls"
        
        print(URL)
        
        Alamofire.request(URL).responseString { (response) -> Void in
            
            switch response.result {
            case .success:
                
                var pullRequest = [PullRequest]()
                
                if let stringResponse = response.result.value {
                    pullRequest = Mapper<PullRequest>().mapArray(JSONString: stringResponse)!
                }
                
                callback(pullRequest, nil)
                
                break
            case .failure(let error):
                print(error)
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailIdentier, for: indexPath) as! DetailTableViewCell

        let pullrequest = feedList[indexPath.row]
        cell.setup(model: pullrequest)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        
        if let url = feedList[row].html_url {
            
            UIApplication.shared.openURL(NSURL(string: url)! as URL)
        }
    }

}
