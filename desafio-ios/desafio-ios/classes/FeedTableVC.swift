//
//  FeedTableVC.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class FeedTableVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableview: UITableView!
    
    let cellIdentifier = "repositoreisCell"
    
    let detailIdentier = "sgDetail"
    
    var feedList:[Repositories] = []
    
    var pageCurrent = 1
    
    var indicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableview.estimatedRowHeight = 180

        self.tableview.rowHeight = UITableViewAutomaticDimension
        
        self.tableview.dataSource = self
        
        self.tableview.delegate = self
        
        self.tableview.tableFooterView = UIView()
        
        activityIndicator()
        
        indicator.startAnimating()
        
        indicator.backgroundColor = UIColor.white
        
        loadRepository(page: pageCurrent)
    }
    
    func loadRepository(page: Int) {
        
        loadData(currentPage: page, callback: { list, error in
            
            if list.count > 0 {
                
                for (item) in list {
                    
                   self.feedList.append(item)
                }
                
                self.tableview.reloadData()
                self.indicator.stopAnimating()
                self.indicator.hidesWhenStopped = true

            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableview.reloadData()
        
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }

    func loadData(currentPage: Int, callback: @escaping (_ repositories: [Repositories], _ error: NSError?) -> ()) -> () {
        
        let URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(currentPage)"

        Alamofire.request(URL).responseJSON { (response) -> Void in
            
            switch response.result {
            case .success:
                
                var repositories = [Repositories]()
                
                if let fullJSON = response.result.value! as? [String:AnyObject] {
                    repositories = Mapper<Repositories>().mapArray(JSONObject: fullJSON["items"])!
                }
            
                callback(repositories, nil)
                
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as!RepositoreisTVCell
        
        let repository = feedList[indexPath.row]
        cell.setup(model: repository)
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (endScrolling >= scrollView.contentSize.height){
            self.pageCurrent += 1
            self.loadRepository(page: pageCurrent)

        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == detailIdentier {
            let detailsVC = segue.destination as! DetailsTableVC
            let path = self.tableview.indexPathForSelectedRow!
            let feed = feedList[path.row]
            detailsVC.repository = feed as Repositories
            
        }
    }
}
