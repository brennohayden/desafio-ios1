//
//  Owner.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class Owner: Mappable {
    
    var id: Int?
    var login: String?
    var avatar: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        avatar <- map["avatar_url"]
    }

}
