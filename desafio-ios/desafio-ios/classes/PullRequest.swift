//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class PullRequest: Mappable {

    var title: String?
    var body: String?
    var createdAt: String?
    var html_url: String?
    var owner: Owner?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        createdAt <- map["created_at"]
        html_url <- map["html_url"]
        owner <- map["user"]
    }

}
