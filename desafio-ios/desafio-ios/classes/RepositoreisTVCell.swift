//
//  RepositoreisTVCell.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoreisTVCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var lbFullname: UILabel!
    @IBOutlet weak var lbForkCount: UILabel!
    @IBOutlet weak var lbStarCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(model: Repositories) {
        
        lbName.text = model.name
        lbDesc.text = model.desc
        
        imgAvatar.sd_setImage(with: URL(string: (model.owner?.avatar)!), placeholderImage: UIImage(named: "avatar"))
        
        lbFullname.text = model.fullname
        lbUsername.text = model.owner?.login
        lbForkCount.text = "\(model.forks!)"
        lbStarCount.text = "\(model.star!)"
    
    }
}
