//
//  Repositories.swift
//  desafio-ios
//
//  Created by Brenno on 21/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class Repositories: Mappable {

    var name: String?
    var fullname: String?
    var desc: String?
    var star: Int?
    var forks: Int?
    var owner: Owner?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        fullname <- map["full_name"]
        desc <- map["description"]
        star <- map["stargazers_count"]
        forks <- map["forks_count"]
        owner <- map["owner"]
    }
}

