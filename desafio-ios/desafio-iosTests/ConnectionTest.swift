//
//  ConnectionTest.swift
//  desafio-ios
//
//  Created by Brenno on 22/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_ios

class ConnectionTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRepositories() {
        
        let feed = FeedTableVC()
        
        let exp = expectation(description: "apiRepo")
        
        feed.loadData(currentPage: 1, callback: { list, error in
            
            if list.count > 0 {
                
                XCTAssertTrue(true)
            
            }else {
                XCTFail()
            }
            exp.fulfill()
        })
        
        self.waitForExpectations(timeout: 5, handler: { error in
            
            if error != nil {
                XCTFail()
            }
            
        })
    }
    
    func testPullRequestFail() {
        
        let pull = DetailsTableVC()
        
        let json = "{ \"name\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        pull.repository = repositories?.first

        let exp = expectation(description: "apiPull")
        
        pull.loadData { (list, error) in
            
            if list.count > 0 {
                
                XCTAssertTrue(true)
                
            }else {
                XCTFail()
            }
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 5, handler: { error in
            
            if error != nil {
                XCTFail()
            }
            
        })
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
