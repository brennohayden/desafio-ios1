//
//  OwnerTest.swift
//  desafio-ios
//
//  Created by Brenno on 22/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_ios

class OwnerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testMapper() {
        let json = "{" +
        "\"login\": \"vanniktech\"," +
        "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\"" +
        "}"
        
        let owner = Mapper<Owner>().mapArray(JSONString: json)
        
        XCTAssertNotNil(owner, "Owner is not null")
        XCTAssertNotNil(owner?.first, "First elements ir not nil")
        XCTAssertEqual(owner?.first?.login, "vanniktech", "Login is equals")
        XCTAssertEqual(owner?.first?.avatar, "https://avatars.githubusercontent.com/u/5759366?v=3", "avatar is equals")

    }
    
    func testLoginFail() {
        let json = "{" +
            "\"login1\": \"vanniktech\"," +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\"" +
        "}"
        
        let owner = Mapper<Owner>().mapArray(JSONString: json)
        
        XCTAssertNotNil(owner, "Owner is not null")
        XCTAssertNotNil(owner?.first, "First elements ir not nil")
        XCTAssertNil(owner?.first?.login)
    }
    
    func testAvatarFail() {
        let json = "{" +
            "\"login\": \"vanniktech\"," +
            "\"avatarurl\": \"https://avatars.githubusercontent.com/u/5759366?v=3\"" +
        "}"
        
        let owner = Mapper<Owner>().mapArray(JSONString: json)
        
        XCTAssertNotNil(owner, "Owner is not null")
        XCTAssertNotNil(owner?.first, "First elements ir not nil")
        XCTAssertNil(owner?.first?.avatar)
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
