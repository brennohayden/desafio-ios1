//
//  PullRequestTest.swift
//  desafio-ios
//
//  Created by Brenno on 22/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import XCTest
import SwiftyJSON
import ObjectMapper

@testable import desafio_ios

class PullRequestTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMapper() {
        
        let json = "{" +
            "\"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/4934\", " +
            "\"html_url\": \"https://github.com/ReactiveX/RxJava/pull/4934\", " +
            "\"title\": \"2.x: Unify CompositeDisposable error messages\", " +
            "\"user\": { " +
                "\"login\": \"vanniktech\", " +
                "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\", " +
            "}," +
            "\"body\": \"Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly\", " +
            "\"created_at\": \"2016-12-21T09:38:48Z\"" +
        "}"

        let pullRequest = Mapper<PullRequest>().mapArray(JSONString: json)
        
        XCTAssertNotNil(pullRequest, "pullRequests is not null")
        XCTAssertNotNil(pullRequest?.first, "First elements ir not nil")
        XCTAssertEqual(pullRequest?.first?.title, "2.x: Unify CompositeDisposable error messages", "errou")
        XCTAssertEqual(pullRequest?.first?.body, "Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly", "body is equals")
        XCTAssertEqual(pullRequest?.first?.createdAt, "2016-12-21T09:38:48Z", "Date is Equals")
        XCTAssertEqual(pullRequest?.first?.html_url, "https://github.com/ReactiveX/RxJava/pull/4934", "html_url is equal")
        XCTAssertEqual(pullRequest?.first?.owner?.login, "vanniktech", "login (owner) equal")
        XCTAssertEqual(pullRequest?.first?.owner?.avatar, "https://avatars.githubusercontent.com/u/5759366?v=3", "avatar (owner) is equal")
    }

    func testTitleFail() {
        
        let json = "{" +
            "\"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/4934\", " +
            "\"html_url\": \"https://github.com/ReactiveX/RxJava/pull/4934\", " +
            "\"title1\": \"2.x: Unify CompositeDisposable error messages\", " +
            "\"user\": { " +
            "\"login\": \"vanniktech\", " +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\", " +
            "}," +
            "\"body\": \"Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly\", " +
            "\"created_at\": \"2016-12-21T09:38:48Z\"" +
        "}"
        
        let pullRequest = Mapper<PullRequest>().mapArray(JSONString: json)
        
        XCTAssertNotNil(pullRequest, "pullRequests is not null")
        XCTAssertNotNil(pullRequest?.first, "First elements ir not nil")
        XCTAssertNil(pullRequest?.first?.title)
        
    }
    
    func testBodyFail() {
        
        let json = "{" +
            "\"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/4934\", " +
            "\"html_url\": \"https://github.com/ReactiveX/RxJava/pull/4934\", " +
            "\"title\": \"2.x: Unify CompositeDisposable error messages\", " +
            "\"user\": { " +
            "\"login\": \"vanniktech\", " +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\", " +
            "}," +
            "\"boddy\": \"Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly\", " +
            "\"created_at\": \"2016-12-21T09:38:48Z\"" +
        "}"
        
        let pullRequest = Mapper<PullRequest>().mapArray(JSONString: json)
        
        XCTAssertNotNil(pullRequest, "pullRequests is not null")
        XCTAssertNotNil(pullRequest?.first, "First elements ir not nil")
        XCTAssertNil(pullRequest?.first?.body)
    }
 
    func testDateFail() {
        
        let json = "{" +
            "\"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/4934\", " +
            "\"html_url\": \"https://github.com/ReactiveX/RxJava/pull/4934\", " +
            "\"title\": \"2.x: Unify CompositeDisposable error messages\", " +
            "\"user\": { " +
            "\"login\": \"vanniktech\", " +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\", " +
            "}," +
            "\"body\": \"Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly\", " +
            "\"createdat\": \"2016-12-21T09:38:48Z\"" +
        "}"
        
        let pullRequest = Mapper<PullRequest>().mapArray(JSONString: json)
        
        XCTAssertNotNil(pullRequest, "pullRequests is not null")
        XCTAssertNotNil(pullRequest?.first, "First elements ir not nil")
        XCTAssertNil(pullRequest?.first?.createdAt)
    }
    
    func testHTMLFail() {
        
        let json = "{" +
            "\"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/4934\", " +
            "\"httml_url\": \"https://github.com/ReactiveX/RxJava/pull/4934\", " +
            "\"title\": \"2.x: Unify CompositeDisposable error messages\", " +
            "\"user\": { " +
            "\"login\": \"vanniktech\", " +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/5759366?v=3\", " +
            "}," +
            "\"body\": \"Unifies the CompositeDisposable error messages and make them a bit more helpful. Just seeing `d is null` reads oddly\", " +
            "\"created_at\": \"2016-12-21T09:38:48Z\"" +
        "}"
        
        let pullRequest = Mapper<PullRequest>().mapArray(JSONString: json)
        
        XCTAssertNotNil(pullRequest, "pullRequests is not null")
        XCTAssertNotNil(pullRequest?.first, "First elements ir not nil")
        XCTAssertNil(pullRequest?.first?.html_url)
    }
    
    
    func testPerformanceExample() {
    
        
        
        
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}





