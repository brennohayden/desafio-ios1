//
//  RepositoryTest.swift
//  desafio-ios
//
//  Created by Brenno on 22/12/16.
//  Copyright © 2016 Brenno Hayden. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_ios

class RepositoryTest: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testMapper() {
        
        let json = "{ \"name\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertEqual(repositories?.first?.name, "elasticsearch", "name equals")
        XCTAssertEqual(repositories?.first?.desc, "Open Source, Distributed, RESTful Search Engine", "desc equals")
        XCTAssertEqual(repositories?.first?.fullname, "elastic/elasticsearch", "full_name equals")
        XCTAssertEqual(repositories?.first?.forks, 6846, "forks equals")
        XCTAssertEqual(repositories?.first?.star, 19932, "star equals")
        XCTAssertNotNil(repositories?.first?.owner, "Owner is not nil")
        XCTAssertEqual(repositories?.first?.owner?.avatar, "https://avatars.githubusercontent.com/u/6764390?v=3", "Avatar (Owner) equals")
        XCTAssertEqual(repositories?.first?.owner?.login, "elasticv", "Login (Owner) equals")
    }
    
    func testNameFail() {
        
        let json = "{ \"namee\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first)
        XCTAssertNil(repositories?.first?.name)
        
    }
    
    func testDescriptionFail() {
        
        let json = "{ \"name\": \"elasticsearch\", \"descripiton\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertNotEqual(repositories?.first?.name, "elasticsearch1")
        XCTAssertNil(repositories?.first?.desc)
    }
    
    func testFullnameFail() {
        
        let json = "{ \"name\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"ful_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertNotEqual(repositories?.first?.desc, "Open Source, Distributed, RESTful Search Engine.")
        XCTAssertNil(repositories?.first?.fullname)
    }
    
    func testForkFail() {
        
        let json = "{ \"name\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": \"zero\", \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertNotEqual(repositories?.first?.fullname, "elastic/elasticsearchh")
        XCTAssertNil(repositories?.first?.forks)
    }
    
    func testStarFail() {
        
        let json = "{ \"name\": \"elasticsearch\", \"descripiton\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": \"zero\", \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertNotEqual(repositories?.first?.forks, 6847)
        XCTAssertNil(repositories?.first?.star)
    }
    
    func testCount() {
        
        let json = "[ { \"name\": \"elasticsearch\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } }, { \"name\": \"elasticsearch1\", \"description\": \"Open Source, Distributed, RESTful Search Engine\", \"full_name\": \"elastic/elasticsearch1\", \"forks_count\": 6846, \"stargazers_count\": 19932, \"owner\": { \"login\": \"elasticv1\", \"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\" } } ]"
        
        let repositories = Mapper<Repositories>().mapArray(JSONString: json)
        
        XCTAssertNotNil(repositories, "repositories is not null")
        XCTAssertNotNil(repositories?.first, "First elements ir not nil")
        XCTAssertEqual(repositories?.count, 2)
        
    }

}

